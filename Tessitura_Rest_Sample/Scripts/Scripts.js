﻿$(".sendForm").click(function (e) {
    e.preventDefault();
    var parent = $(this).closest(".jumbotron");
    var form = $(this).closest("form");
    var url = form.attr("action");
    var formData = JSON.stringify(form.serializeArray());
    $.ajax({
        type: "POST",
        url: "home" + url,
        data: {
            data: formData
        },
        success: function (data) {
            $(".dvResult", parent).html(data);
        }
    });
});