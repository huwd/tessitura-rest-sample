﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Tessitura.Service.Client.Custom;
using Tessitura.Services.Common.Client;

namespace Tessitura_Rest_Sample.Models
{
    public class rest
    {
        public Dictionary<int, string> results = new Dictionary<int, string>();
        public string restUri = "";
        public string restCredentials = "";
        
        public int newR()
        {
            int r = 0;
            if (results.Count > 0)
            {
                r = results.Last().Key + 1;
            }
            return r;
        }

        public async Task getRest(int r, string endpoint)
        {
            string credentials = restCredentials;
            string baseUri = restUri;
            if (!baseUri.EndsWith("/"))
            {
                baseUri += "/";
            }

            var authHeaderString = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(credentials));
            var httpClient = new HttpClient { Timeout = new TimeSpan(0, 0, 5000) };
            httpClient.DefaultRequestHeaders.Add("Accept", "text/json");
            httpClient.DefaultRequestHeaders.Add("Authorization", authHeaderString);

            try
            {
                var response = await httpClient.GetAsync(baseUri + endpoint).ConfigureAwait(continueOnCapturedContext: false);
                results[r] = await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                results[r] = ex.Message;
            }
        }

        public async Task postRest(int r, string endpoint, object variables)
        {
            string credentials = restCredentials;
            string baseUri = restUri;
            if (!baseUri.EndsWith("/"))
            {
                baseUri += "/";
            }

            var authHeaderString = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(credentials));
            var httpClient = new HttpClient { Timeout = new TimeSpan(0, 0, 5000) };
            httpClient.DefaultRequestHeaders.Add("Accept", "text/json");
            httpClient.DefaultRequestHeaders.Add("Authorization", authHeaderString);

            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(variables), Encoding.UTF8, "application/json");
                var response = await httpClient.PostAsync(baseUri + endpoint, content).ConfigureAwait(continueOnCapturedContext: false);
                results[r] = await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                results[r] = ex.Message;
            }
        }

        public JArray customRest(int id, string parameters)
        {
            try
            {
                LocalProcedureRequest lpr = new LocalProcedureRequest()
                {
                    Parameters = parameters,
                    ProcedureId = id
                };

                int r = newR();
                var t = postRest(r, "Custom/Execute", lpr);
                t.Wait();

                string rtn = results[r].Trim();
                results.Remove(r);

                return (JArray)JToken.Parse(rtn)["Table"];
            }
            catch (Exception)
            {
                return null;
            }
        }

        public JArray batchRest(List<Request> reqs)
        {
            JArray rtn = new JArray();
            var vars = new BatchRequest()
            {
                Requests = reqs
            };

            int r = newR();
            var t = postRest(r, "Batch", vars);
            try
            {
                t.Wait();
                rtn = JArray.Parse("[" + results[r] + "]");
                results.Remove(r);
            }
            catch (Exception) { }

            return rtn;
        }
    }
}