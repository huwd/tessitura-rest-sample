﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Tessitura.Service.Client.Web;
using Tessitura.Services.Common.Client;
using Tessitura_Rest_Sample.Models;

namespace Tessitura_Rest_Sample.Controllers
{
    public class HomeController : Controller
    {
        rest rst;

        public ActionResult Index()
        {
            return View();
        }

        public void initRest()
        {
            rst = new rest();
            rst.restUri = WebConfigurationManager.AppSettings["Rest_Uri"];
            rst.restCredentials = WebConfigurationManager.AppSettings["Rest_Credentials"];
        }

        public string getrest_example(string data)
        {
            string rtn = "";

            try
            {
                initRest();

                JArray jData = JArray.Parse(data);
                string endpoint = "";
                foreach (JObject p in jData)
                {
                    switch (p["name"].ToString())
                    {
                        case "txtGetRest":
                            endpoint = p["value"].ToString();
                            break;
                    }
                }

                int r = rst.newR();

                var t = rst.getRest(r, endpoint);
                t.Wait();

                rtn = rst.results[r];
                rst.results.Remove(r);
            }
            catch (Exception ex) {
                rtn = ex.Message;
            }

            return rtn;
        }

        public string postrest_example(string data)
        {
            string rtn = "";

            try
            {
                initRest();

                int r = rst.newR();

                var sessionVars = new SessionRequest()
                {
                    IpAddress = Request.ServerVariables["REMOTE_HOST"]
                };

                var t = rst.postRest(r, "Web/Session", sessionVars);
                t.Wait();

                rtn = rst.results[r];
                rst.results.Remove(r);
            }
            catch (Exception ex) {
                rtn = ex.Message;
            }

            return rtn;
        }

        public string custrest_example(string data)
        {
            string rtn = "";

            try
            {
                initRest();

                JArray jData = JArray.Parse(data);
                int id = 0;
                string paramsStr = "";

                foreach (JObject p in jData)
                {
                    switch (p["name"].ToString())
                    {
                        case "txtCustID":
                            int.TryParse(p["value"].ToString(), out id);
                            break;
                        case "txtCustParams":
                            paramsStr = p["value"].ToString();
                            break;
                    }
                }
                
                rtn = rst.customRest(id, paramsStr).ToString();
            }
            catch (Exception ex) {
                rtn = ex.Message;
            }

            return rtn;
        }

        public string batchrest_example(string data)
        {
            string rtn = "";

            try
            {
                initRest();

                JArray jData = JArray.Parse(data);
                string batchStr = "";

                foreach (JObject p in jData)
                {
                    switch (p["name"].ToString())
                    {
                        case "txtBatch":
                            batchStr = p["value"].ToString();
                            break;
                    }
                }
                string[] delimiter = new string[] { "\r\n" };
                string[] reqsStr = batchStr.Split(delimiter, StringSplitOptions.None);

                List<Request> reqs = new List<Request>();
                foreach (string str in reqsStr)
                {
                    reqs.Add(new Request() { Id = reqs.Count + 1, HttpMethod = "GET", Uri = rst.restUri + str, ContinueOnError = true });
                }

                rtn = rst.batchRest(reqs).ToString();
            }
            catch (Exception ex) {
                rtn = ex.Message;
            }

            return rtn;
        }
    }
}